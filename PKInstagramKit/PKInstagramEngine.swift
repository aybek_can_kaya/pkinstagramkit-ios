//
//  PKInstagramEngine.swift
//  PKInstagramKit
//
//  Created by Gokhan Gultekin on 11/10/2016.
//  Copyright © 2016 Peakode. All rights reserved.
//

import UIKit

class PKInstagramEngine: NSObject {
    
    class func oauthURL() -> URL {
    
        let baseURL: String = "https://api.instagram.com/oauth/authorize/?"
        let clientId: String = "095d1b108c4b43df8449feb46cf794cd"
        let redirectUri: String = "http://yourcallback.com/?this=that"
        
        //do not change these variables if you don't need them
        let responseType: String = "token"
        let scope: String = "basic"
        
        let urlString: String = baseURL + "client_id=" + clientId +
            "&" + "redirect_uri=" + redirectUri +
            "&" + "response_type=" + responseType +
            "&" + "scope=" + scope
        
        let oauthURL: URL = URL.init(string: urlString)!
        
        return oauthURL
    
    }
    
    class func removeLocalData() {
    
        
        URLCache.shared.removeAllCachedResponses()
        
        let storage = HTTPCookieStorage.shared
        for cookie in storage.cookies! {
            storage.deleteCookie(cookie)
        }
        
        UserDefaults.standard.removeObject(forKey: "access_token")

    }
    
    class func saveAccessToken(token: String) {
    
        UserDefaults.standard.set(token, forKey: "access_token")
    }
    
    class func accessToken() -> String {
        
        var tokenString: String = ""
        
        if let token = UserDefaults.standard.object(forKey: "access_token") {
            
            tokenString = token as! String
        }
        
        return tokenString
        
    }
    
}
